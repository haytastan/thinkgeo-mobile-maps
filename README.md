# ThinkGeo Mobile Maps

If you're new to ThinkGeo's Mobile Maps, we suggest you download ThinkGeo Maps from the App Store (iOS) or Google Play (Android). This app offers around 100 'HowDoI' samples showcasing ThinkGeo's Xamarin Mapping Components. The source code of this app can be found [here](https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/tree/master/samples/xamarin-forms/HowDoISample). 

[<img src="https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/raw/master/quick-start-guide/assets/Apple_Store_Badge.png"  width="180" height="60">](https://apps.apple.com/us/app/igis/id1559817900) [<img src="https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/raw/master/quick-start-guide/assets/Google_Play_Badge.png"  width="180" height="60">](https://play.google.com/store/apps/details?id=com.thinkgeo.androidhowdoi)

## Repository Layout

- `/quick-start-guide`: A guide to quickly get started.

- `/samples/xamarin-forms`: A collection of "How Do I" samples, showcasing around 100 features in a user-friendly application.

- `/docs`: Offline API documentation in Markdown format.

## Quick Start Guide
The Quick Start Guide shows you how to create a map in a Xamarin.Forms app using ThinkGeo Mobile Maps. By the end, you'll grasp the basics of Mobile Maps controls.

- [ThinkGeo Mobile Quickstart Guide](https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/tree/master/quick-start-guide?ref_type=heads)

## HowDoI Samples

The ThinkGeo HowDoI Samples feature approximately 100 straightforward examples that demonstrate the capabilities of ThinkGeo's .NET Xamarin Mapping Components. You can use these samples as a foundation for your own application or refer to them to understand how to use our controls following best practices.

- [ThinkGeo Xamarin.Forms Samples](https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/tree/master/samples/xamarin-forms/HowDoISample?ref_type=heads)


## More Resources:
- [ThinkGeo Mobile Edition Online Docs](https://docs.thinkgeo.com/products/mobile-maps/quickstart/) 
    
- [ThinkGeo Forum](https://community.thinkgeo.com/c/thinkgeo-ui-for-mobile/)
        
- [ThinkGeo Blog](https://www.thinkgeo.com/blog/) 
