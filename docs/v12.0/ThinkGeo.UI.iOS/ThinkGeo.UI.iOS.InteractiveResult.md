# InteractiveResult


## Inheritance Hierarchy

+ `Object`
  + **`InteractiveResult`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`InteractiveResult()`](#interactiveresult)|
|[`InteractiveResult(InteractiveOverlayDrawType,ProcessOtherOverlaysMode)`](#interactiveresultinteractiveoverlaydrawtypeprocessotheroverlaysmode)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`DrawThisOverlay`](#drawthisoverlay)|[`InteractiveOverlayDrawType`](ThinkGeo.UI.iOS.InteractiveOverlayDrawType.md)|Gets or sets the DrawThisOverlay type to indicicate to draw this overlay or not.|
|[`NewCurrentExtent`](#newcurrentextent)|[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)|Gets or sets the new current extent if the interative action affect the extent.|
|[`ProcessOtherOverlaysMode`](#processotheroverlaysmode)|[`ProcessOtherOverlaysMode`](ThinkGeo.UI.iOS.ProcessOtherOverlaysMode.md)|Gets or sets the DrawThisOverlay type to indicicate how to process other overlays.|
|[`TransformInfo`](#transforminfo)|[`TransformArguments`](ThinkGeo.UI.iOS.TransformArguments.md)|Gets or sets the transform information.|
|[`Velocity`](#velocity)|`CGPoint`|Gets or sets the velocity.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`InteractiveResult()`](#interactiveresult)|
|[`InteractiveResult(InteractiveOverlayDrawType,ProcessOtherOverlaysMode)`](#interactiveresultinteractiveoverlaydrawtypeprocessotheroverlaysmode)|

### Protected Constructors


### Public Properties

#### `DrawThisOverlay`

**Summary**

   *Gets or sets the DrawThisOverlay type to indicicate to draw this overlay or not.*

**Remarks**

   *N/A*

**Return Value**

[`InteractiveOverlayDrawType`](ThinkGeo.UI.iOS.InteractiveOverlayDrawType.md)

---
#### `NewCurrentExtent`

**Summary**

   *Gets or sets the new current extent if the interative action affect the extent.*

**Remarks**

   *N/A*

**Return Value**

[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)

---
#### `ProcessOtherOverlaysMode`

**Summary**

   *Gets or sets the DrawThisOverlay type to indicicate how to process other overlays.*

**Remarks**

   *N/A*

**Return Value**

[`ProcessOtherOverlaysMode`](ThinkGeo.UI.iOS.ProcessOtherOverlaysMode.md)

---
#### `TransformInfo`

**Summary**

   *Gets or sets the transform information.*

**Remarks**

   *N/A*

**Return Value**

[`TransformArguments`](ThinkGeo.UI.iOS.TransformArguments.md)

---
#### `Velocity`

**Summary**

   *Gets or sets the velocity.*

**Remarks**

   *N/A*

**Return Value**

`CGPoint`

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


