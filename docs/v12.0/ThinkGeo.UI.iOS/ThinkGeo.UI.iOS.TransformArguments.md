# TransformArguments


## Inheritance Hierarchy

+ `Object`
  + **`TransformArguments`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`TransformArguments()`](#transformarguments)|
|[`TransformArguments(Single,Single,Single,RectangleShape,Single,Single)`](#transformargumentssinglesinglesinglerectangleshapesinglesingle)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`CenterX`](#centerx)|`nfloat`|Gets or sets the center x of MapControl.|
|[`CenterY`](#centery)|`nfloat`|Gets or sets the center y of MapControl.|
|[`InertiallyPan`](#inertiallypan)|`Boolean`|Gets or sets a value indicating whether [inertially pan].|
|[`ProcessingExtent`](#processingextent)|[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)|Gets or sets the processing extent of MapControl.|
|[`Rotation`](#rotation)|`Single`|Gets or sets the rotation of MapControl.|
|[`Scale`](#scale)|`Single`|Gets or sets the scale of MapControl.|
|[`X`](#x)|`nfloat`|Gets or sets the x of MapControl.|
|[`Y`](#y)|`nfloat`|Gets or sets the y of MapControl.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`PostScale(Single,nfloat,nfloat)`](#postscalesinglenfloatnfloat)|
|[`PostTranslate(nfloat,nfloat)`](#posttranslatenfloatnfloat)|
|[`Reset()`](#reset)|
|[`Set(TransformArguments)`](#settransformarguments)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`TransformArguments()`](#transformarguments)|
|[`TransformArguments(Single,Single,Single,RectangleShape,Single,Single)`](#transformargumentssinglesinglesinglerectangleshapesinglesingle)|

### Protected Constructors


### Public Properties

#### `CenterX`

**Summary**

   *Gets or sets the center x of MapControl.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `CenterY`

**Summary**

   *Gets or sets the center y of MapControl.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `InertiallyPan`

**Summary**

   *Gets or sets a value indicating whether [inertially pan].*

**Remarks**

   *N/A*

**Return Value**

`Boolean`

---
#### `ProcessingExtent`

**Summary**

   *Gets or sets the processing extent of MapControl.*

**Remarks**

   *N/A*

**Return Value**

[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)

---
#### `Rotation`

**Summary**

   *Gets or sets the rotation of MapControl.*

**Remarks**

   *N/A*

**Return Value**

`Single`

---
#### `Scale`

**Summary**

   *Gets or sets the scale of MapControl.*

**Remarks**

   *N/A*

**Return Value**

`Single`

---
#### `X`

**Summary**

   *Gets or sets the x of MapControl.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `Y`

**Summary**

   *Gets or sets the y of MapControl.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `PostScale(Single,nfloat,nfloat)`

**Summary**

   *Posts the scale of MapControl.*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|scale|`Single`|The scale.|
|centerX|`nfloat`|The center x.|
|centerY|`nfloat`|The center y.|

---
#### `PostTranslate(nfloat,nfloat)`

**Summary**

   *Posts the translate of MapControl.*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|offsetX|`nfloat`|The offset x.|
|offsetY|`nfloat`|The offset y.|

---
#### `Reset()`

**Summary**

   *Resets this instance of MapControl.*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `Set(TransformArguments)`

**Summary**

   *Sets the specified matrix of MapControl.*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|matrix|[`TransformArguments`](ThinkGeo.UI.iOS.TransformArguments.md)|The matrix.|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


