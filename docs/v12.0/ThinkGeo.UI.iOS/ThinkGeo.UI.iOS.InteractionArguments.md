# InteractionArguments


## Inheritance Hierarchy

+ `Object`
  + **`InteractionArguments`**

## Members Summary

### Public Constructors Summary


|Name|
|---|
|[`InteractionArguments()`](#interactionarguments)|

### Protected Constructors Summary


|Name|
|---|
|N/A|

### Public Properties Summary

|Name|Return Type|Description|
|---|---|---|
|[`CurrentExtent`](#currentextent)|[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)|Gets or sets the current extent.|
|[`CurrentResolution`](#currentresolution)|`Double`|Gets or sets the current resolution.|
|[`MapHeight`](#mapheight)|`nfloat`|Gets or sets the height of the map.|
|[`MapUnit`](#mapunit)|[`GeographyUnit`](../ThinkGeo.Core/ThinkGeo.Core.GeographyUnit.md)|Gets or sets the map unit.|
|[`MapWidth`](#mapwidth)|`nfloat`|Gets or sets the width of the map.|
|[`Scale`](#scale)|`Double`|Gets or sets the scale.|
|[`ScreenPointers`](#screenpointers)|Collection<`CGPoint`>|Gets the screen pointers.|
|[`ScreenX`](#screenx)|`nfloat`|Gets or sets the screen x.|
|[`ScreenY`](#screeny)|`nfloat`|Gets or sets the screen y.|
|[`SearchingTolerance`](#searchingtolerance)|`Double`|Gets or sets the searching tolerance.|
|[`Velocity`](#velocity)|`CGPoint`|Gets or sets the velocity.|
|[`WorldX`](#worldx)|`Double`|Gets or sets the world x.|
|[`WorldY`](#worldy)|`Double`|Gets or sets the world y.|
|[`ZoomLevelSet`](#zoomlevelset)|[`ZoomLevelSet`](../ThinkGeo.Core/ThinkGeo.Core.ZoomLevelSet.md)|Gets or sets the zoom level set.|

### Protected Properties Summary

|Name|Return Type|Description|
|---|---|---|
|N/A|N/A|N/A|

### Public Methods Summary


|Name|
|---|
|[`Equals(Object)`](#equalsobject)|
|[`GetHashCode()`](#gethashcode)|
|[`GetType()`](#gettype)|
|[`ToString()`](#tostring)|

### Protected Methods Summary


|Name|
|---|
|[`Finalize()`](#finalize)|
|[`MemberwiseClone()`](#memberwiseclone)|

### Public Events Summary


|Name|Event Arguments|Description|
|---|---|---|
|N/A|N/A|N/A|

## Members Detail

### Public Constructors


|Name|
|---|
|[`InteractionArguments()`](#interactionarguments)|

### Protected Constructors


### Public Properties

#### `CurrentExtent`

**Summary**

   *Gets or sets the current extent.*

**Remarks**

   *N/A*

**Return Value**

[`RectangleShape`](../ThinkGeo.Core/ThinkGeo.Core.RectangleShape.md)

---
#### `CurrentResolution`

**Summary**

   *Gets or sets the current resolution.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `MapHeight`

**Summary**

   *Gets or sets the height of the map.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `MapUnit`

**Summary**

   *Gets or sets the map unit.*

**Remarks**

   *N/A*

**Return Value**

[`GeographyUnit`](../ThinkGeo.Core/ThinkGeo.Core.GeographyUnit.md)

---
#### `MapWidth`

**Summary**

   *Gets or sets the width of the map.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `Scale`

**Summary**

   *Gets or sets the scale.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `ScreenPointers`

**Summary**

   *Gets the screen pointers.*

**Remarks**

   *N/A*

**Return Value**

Collection<`CGPoint`>

---
#### `ScreenX`

**Summary**

   *Gets or sets the screen x.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `ScreenY`

**Summary**

   *Gets or sets the screen y.*

**Remarks**

   *N/A*

**Return Value**

`nfloat`

---
#### `SearchingTolerance`

**Summary**

   *Gets or sets the searching tolerance.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `Velocity`

**Summary**

   *Gets or sets the velocity.*

**Remarks**

   *N/A*

**Return Value**

`CGPoint`

---
#### `WorldX`

**Summary**

   *Gets or sets the world x.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `WorldY`

**Summary**

   *Gets or sets the world y.*

**Remarks**

   *N/A*

**Return Value**

`Double`

---
#### `ZoomLevelSet`

**Summary**

   *Gets or sets the zoom level set.*

**Remarks**

   *N/A*

**Return Value**

[`ZoomLevelSet`](../ThinkGeo.Core/ThinkGeo.Core.ZoomLevelSet.md)

---

### Protected Properties


### Public Methods

#### `Equals(Object)`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Boolean`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|obj|`Object`|N/A|

---
#### `GetHashCode()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Int32`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `GetType()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Type`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `ToString()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`String`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Protected Methods

#### `Finalize()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Void`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---
#### `MemberwiseClone()`

**Summary**

   *N/A*

**Remarks**

   *N/A*

**Return Value**

|Type|Description|
|---|---|
|`Object`|N/A|

**Parameters**

|Name|Type|Description|
|---|---|---|
|N/A|N/A|N/A|

---

### Public Events


