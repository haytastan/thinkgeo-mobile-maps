# How Do I Sample for Xamarin.Forms

The “How Do I?” samples collection is a comprehensive set containing dozens of interactive samples. Available in C#, these samples are designed to hit all the highlights of ThinkGeo UI, from simply adding a layer to a map to performing spatial queries and applying a thematic style. Consider this collection your “encyclopedia” of all the ThinkGeo UI basics and a great starting place for new users.

This sample has been deployed in App Store(iOS) and Google Play(Android), please download it from the following links if you are interested.

[<img src="https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/raw/master/quick-start-guide/assets/Apple_Store_Badge.png"  width="180" height="60">](https://apps.apple.com/us/app/igis/id1559817900) [<img src="https://gitlab.com/thinkgeo/public/thinkgeo-mobile-maps/-/raw/master/quick-start-guide/assets/Google_Play_Badge.png"  width="180" height="60">](https://play.google.com/store/apps/details?id=com.thinkgeo.androidhowdoi)

![Screenshot](Screenshot.gif)
