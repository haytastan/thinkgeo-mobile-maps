﻿namespace HowDoISample.Models
{
    public class SampleMenuItem
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }
    }
}