﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HowDoISample.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoadingPage
    {
        public LoadingPage()
        {
            InitializeComponent();
        }
    }
}